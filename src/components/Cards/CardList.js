import React from 'react';
import Card from '../Cards/Card';

const CardList = ({filteredProjects, projects, setProjects}) => {

    return (
        <div>
            <ul>
                {filteredProjects.map(project => (
                    <Card key={project.id}
                        text={project.text}
                        deadline={project.deadline}
                        description={project.description}
                        project={project}
                        projects={projects}
                        setProjects={setProjects}
                    />
                ))}
            </ul>
        </div>

    );
};

export default CardList
