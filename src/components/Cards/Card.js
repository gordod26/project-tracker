import React from "react";

const Card = ({setProjects, projects, project, text, key, description, deadline, completedProjects, setCompletedProjects, incompleteProject, setIncompleteProjects}) => {

    const deleteHandler = () => {
        setProjects(projects.filter((el) => (el.id !== project.id)))
    };

    const completeHandler = () => {
        setProjects(projects.map(item => {
            if (item.id === project.id) {
                return {
                    ...item,
                    completed: !item.completed,
                }
            }
            return item
        }))
    };

    return (
        <div>
            <li>{text}
                <br />
                {description}
                <br />
                {deadline}
            </li>
            <button onClick={completeHandler}>complete</button>
            <button onClick={deleteHandler}>trash</button>
        </div>
    )
}

export default Card
