import React from 'react';


const CreateNewForm = ({setStatus, status, setInputText, inputText, projects, setProjects, descriptionText, setDescriptionText, dueText, setDueText, createdText, setCreatedText, }) => {

    const inputTextHandler = (e) => {
        console.log(e.target.value)
        setInputText(e.target.value);
    }
    const descriptionTextHandler = (e) => {
        console.log(e.target.value)
        setDescriptionText(e.target.value);
    }
    const createdTextHandler = (e) => {
        console.log(e.target.value)
        setCreatedText(e.target.value);
    }
    const dueTextHandler = (e) => {
        console.log(e.target.value)
        setDueText(e.target.value);
    }
    const submitProjectHandler = (e) => {
        e.preventDefault();
        setProjects([
            ...projects, {text: inputText, description: descriptionText, completed: false, deadline: dueText, createdDate: createdText, id: Math.random() * 1000}
        ])
        console.log(projects)
        setInputText("");
        setDescriptionText("");
        setCreatedText("");
        setDueText("");
    }

    const statusHandler = (e) => {
        setStatus(e.target.value)
    }

    return (
        <form>

            <label for='newProject'>New Project:</label>
            <input id='newProject' type="text" value={inputText} onChange={inputTextHandler} />
            <br />
            <label for='description'>Description:</label>
            <input id='description' type="text" value={descriptionText} onChange={descriptionTextHandler} />
            <br />
            <label for='createdOn'>Created:</label>
            <input id='createdOn' type="text" value={createdText} onChange={createdTextHandler} />
            <br />
            <label for='due'>Due:</label>
            <input id='due' type="text" value={dueText} onChange={dueTextHandler} />
            <br />
            <button className="todo-button" type="submit" onClick={submitProjectHandler}>
                submit
                <i className="fas fa-plus-square"></i>
            </button>
            <div>
                <select onChange={statusHandler} name='projects'>
                    <option value='all'>All</option>
                    <option value='completed'>Completed</option>
                    <option value='uncompleted'>Uncompleted</option>
                </select>
            </div>
        </form>
    )
};


export default CreateNewForm
