import './App.css';
import React from "react";
import {useState, useEffect} from "react";
import Button from "./components/CNP/button"
import CardList from "./components/Cards/CardList"

function App() {
    const [inputText, setInputText] = useState("");
    const [descriptionText, setDescriptionText] = useState("")
    const [dueText, setDueText] = useState("")
    const [createdText, setCreatedText] = useState("")
    const [projects, setProjects] = useState([])
    const [filteredProjects, setFilteredProjects] = useState([])
    const [status, setStatus] = useState('all')


    const filterHandler = () => {
        switch (status) {
            case 'completed':
                setFilteredProjects(projects.filter(proj => proj.completed === true))
                break;
            case 'uncompleted':
                setFilteredProjects(projects.filter(proj => proj.completed === false))
                break;
            default:
                setFilteredProjects(projects);
                break;
        }
    }

    useEffect(() => {filterHandler()}, [status, projects])

    return (
        <div>
            <Button inputText={inputText}
                setInputText={setInputText}
                projects={projects}
                setProjects={setProjects}
                descriptionText={descriptionText}
                setDescriptionText={setDescriptionText}
                dueText={dueText}
                setDueText={setDueText}
                createdText={createdText}
                setCreatedText={setCreatedText}
                status={status}
                setStatus={setStatus}
            />
            <CardList
                projects={projects}
                setProjects={setProjects}
                filteredProjects={filteredProjects}
                setFilteredProjects={setFilteredProjects}
            />
        </div>
    )
}

export default App;
